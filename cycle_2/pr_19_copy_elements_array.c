#include<stdio.h>
void main() {
	
	int arr[4] = {2,3,5,7};
	int arr_b[4];

	for(int i=0; i < 4; i++)
      	arr_b[i] = arr[i];

	printf("first array : ");
	printf("[");
	for(int i=0; i < 4; i++)
      	printf(" %d, ", arr[i]);
	printf("] \n");

	printf("Copied array : ");
	printf("[");
	for(int i=0; i < 4; i++)
      	printf(" %d, ", arr_b[i]);
	printf("] \n");
	

}
