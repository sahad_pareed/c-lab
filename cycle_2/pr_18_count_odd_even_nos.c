#include<stdio.h>
void main() {
  int nodd=0, neven=0, num, digit;
  printf("Enter a number : ");
  scanf("%d",&num);
  while (num > 0) {
   digit = num % 10; 
   if (digit % 2 == 1)
     nodd++;
   else neven++;
   num /= 10; 
 }
 printf("Odd digits : %d \nEven digits: %d\n", nodd, neven);
}
