#include<stdio.h>
int min(int ar[10],int);
int max(int ar[10],int);
void main()
{
	int a[10],n,i,mini,maxi;
	printf("Enter the limit : ");
	scanf("%d",&n);
	printf("Enter the array elements : ");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	mini=min(a,n);
	maxi=max(a,n);
	printf("The smallest element in the array is %d \n",mini);
	printf("The largest element in the array is %d ",maxi);
}
int min(int ar[10],int n)
{
	int s=ar[0];
	for(int i=1;i<n;i++)
	{
		if(s>ar[i])
			s=ar[i];
	}
	return (s);
}
int max(int ar[10],int n)
{
	int s=ar[0];
	for(int i=1;i<n;i++)
	{
		if(s<ar[i])
			s=ar[i];
	}
	return (s);
}
