#include<stdio.h>
void main()
{
	int i;
	float f;
	double d;
	char c;
	printf("Size of int: %zu bytes \n", sizeof(i));
	printf("Size of float: %zu bytes \n", sizeof(f));
	printf("Size of double: %zu bytes \n", sizeof(d));
	printf("Size of char: %zu bytes \n", sizeof(c));
}
