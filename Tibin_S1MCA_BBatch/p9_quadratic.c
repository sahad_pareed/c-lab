#include<stdio.h>
#include<math.h>
void main()
{
	int a,b,c,d;
	float root1,root2;
	printf("Enter the a,b,c for ax^2+bx+c : ");
	scanf("%d %d %d",&a,&b,&c);
	if(a==0)
	{
		printf("Infinity");
	}
	else
	{
	d=(b*b)-(4*a*c);
	if(d>0)
	{
		root1=(-b+sqrt(d))/(2*a);
		root2=(-b-sqrt(d))/(2*a);
		printf("The roots are real root1 : %f  root2 : %f ",root1,root2);
	}
	else if(d==0)
	{
		root1=-b/(2*a);
		printf("Both roots are real and equal, root is %f",root1);
	}
	else
	{
		printf("Both roots are imaginary ");
	}
	}
}
