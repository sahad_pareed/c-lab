#include<stdio.h>
#include<math.h>

int Fibonacci(int);

void main(){
	int limit;
	
        printf("Enter a number: ");
        scanf("%d", &limit);
	
	for(int i=1; i<=limit; i++) {
		printf("%d \n", Fibonacci(i));
	}
	

}

int Fibonacci(int n) {
	if(n==0) return 0;
	else if(n==1) return 1;
	else return (Fibonacci(n-1) + Fibonacci(n-2));
}

