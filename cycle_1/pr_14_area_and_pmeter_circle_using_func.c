#include<stdio.h>
#define pi 3.14
double area_fx(int);
double perimeter_fx(int);

void main(){
	int radius;
	double area, perimeter;
	
        printf("Enter Radius Of Circle:");
        scanf("%d", &radius);

	area = area_fx(radius);
	perimeter = perimeter_fx(radius);
	
	printf("Area of Circle is %f \n", area);
	printf("Perimeter of Circle is %f \n", perimeter);
        
}

double area_fx(int rad) {
	return pi*(rad*rad);
}

double perimeter_fx(int rad) {
	return 2*pi*rad;
}
